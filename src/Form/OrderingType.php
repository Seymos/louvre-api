<?php


namespace App\Form;


use App\Entity\Ordering;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'type',
                ChoiceType::class
            )
            ->add(
                'visit_date',
                DateType::HTML5_FORMAT,
                [
                    'format' => "dd/MM/yyyy",
                    'model_timezone' => "Europe/Paris"
                ]
            )
            ->add(
                'email',
                EmailType::class
            )
            ->add(
                'ticket_number',
                IntegerType::class
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ordering::class
        ]);
    }
}