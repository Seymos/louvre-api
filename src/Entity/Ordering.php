<?php


namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Services\CodeGeneratorInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\OrderingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Ordering implements CodeGeneratorInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="type")
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @ORM\Column(type="date", name="visit_date")
     * @Assert\Date()
     * @var string A "Y-m-d" formatted value
     */
    private $visitDate;

    /**
     * @ORM\Column(type="integer", name="ticket_number")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     * @Assert\Range(
     *     min = 1,
     *     max = 1000,
     *     minMessage = "Vous devez indiquer au moins un participant pour valider la commande.",
     *     maxMessage = "Vous dépassez la capacité du musée, merci de saisir moins de participants."
     * )
     */
    private $ticketNumber;

    /**
     *  @ORM\Column(type="string", name="email")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
     * @Assert\Email(
     *     message = "Merci de saisir un email valide."
     *     )
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", name="payed")
     */
    private $payed;

    /**
     * @ORM\Column(type="boolean", name="email_sent")
     */
    private $emailSent;

    /**
     * One Ordering has Many Tickets.
     * @ORM\Column(name="tickets")
     * @ORM\OneToMany(targetEntity="App\Entity\Ticket", mappedBy="ordering", cascade="persist")
     */
    private $tickets;

    public function __construct()
    {
        $this->payed = false;
        $this->emailSent = false;
        $this->tickets = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getVisitDate()
    {
        return $this->visitDate;
    }

    /**
     * @param mixed $visitDate
     */
    public function setVisitDate($visitDate): void
    {
        $this->visitDate = $visitDate;
    }

    /**
     * @return mixed
     */
    public function getTicketNumber()
    {
        return $this->ticketNumber;
    }

    /**
     * @param mixed $ticketNumber
     */
    public function setTicketNumber($ticketNumber): void
    {
        $this->ticketNumber = $ticketNumber;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * @param mixed $payed
     */
    public function setPayed($payed): void
    {
        $this->payed = $payed;
    }

    /**
     * @return mixed
     */
    public function getEmailSent()
    {
        return $this->emailSent;
    }

    /**
     * @param mixed $emailSent
     */
    public function setEmailSent($emailSent): void
    {
        $this->emailSent = $emailSent;
    }

    /**
     * @return mixed
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @param mixed $tickets
     */
    public function setTickets($tickets): void
    {
        $this->tickets = $tickets;
    }

    public function generate(): string
    {
        return uniqid('ORD_', true);
    }
}