<?php


namespace App\Services;


class CodeGenerator
{
    public static function generate(): string
    {
        return uniqid('ORD_', true);
    }
}
