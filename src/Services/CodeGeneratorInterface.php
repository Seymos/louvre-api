<?php


namespace App\Services;


interface CodeGeneratorInterface
{
    public function generate(): string ;
}