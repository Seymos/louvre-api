<?php


namespace App\Controller;


use App\Entity\Ordering;
use App\Repository\OrderingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route(path="/", name="index")
     * @return JsonResponse
     */
    public function index()
    {
        return new JsonResponse("Hello World from Louvre API ! It works !", Response::HTTP_OK);
    }

    /**
     * @Route(path="/orderings", methods={"POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function createOrdering(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {
        $ordering = $serializer->deserialize($request->getContent(), Ordering::class, 'json');
        $date = new \DateTime(json_decode($request->getContent())->visit_date);
        $tickets = [];
        $serializer->serialize($tickets, []);
        $ordering->setVisitDate($date);
        dd('Ordering: ',$ordering->getTickets());
        $entityManager->persist($ordering);
        $entityManager->flush();
        $data = [
            'message' => "Commande créée, veuillez remplir vos tickets svp !",
            'ordering_id' => $ordering->getId()
        ];
        return new JsonResponse($data, Response::HTTP_CREATED, [], true);
    }

    /**
     * @Route(path="/hello", name="hello")
     * @return JsonResponse
     */
    public function hello()
    {
        return new JsonResponse("Hello you !", Response::HTTP_OK);
    }
}