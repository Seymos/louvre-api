<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191015152616 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
        CREATE TABLE tickets (
        id SERIAL PRIMARY KEY NOT NULL, 
        last_name VARCHAR (50), 
        first_name VARCHAR (50), 
        country VARCHAR (50), 
        birth_date DATE, 
        pricing FLOAT, 
        promotion BOOLEAN NOT NULL
        )
        ');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE tickets');
    }
}
