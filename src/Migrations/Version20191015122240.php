<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191015122240 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
        CREATE TABLE ordering (
        id SERIAL PRIMARY KEY NOT NULL, 
        type VARCHAR(50), 
        visit_date DATE, 
        ticket_number INTEGER, 
        email VARCHAR(255),
        unique_code VARCHAR(255),
        payed BOOLEAN NOT NULL,
        email_sent BOOLEAN NOT NULL,
        tickets json
        )
        ');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE ordering');
    }
}
